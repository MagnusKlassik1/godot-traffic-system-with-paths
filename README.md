### Godot 3 - Traffic System With Random Paths
![header](images/1_header.png)*Screenshot from my truck driving game.*
#### 1 Introduction
Hi everyone! This is how I implemented a Traffic System in Godot 3. I personally used it for my truck game, but I think the system can also be useful in other types of games that need to implement some kind of traffic.

The system is based on the use of the GridMap node, on whose tiles the traffic logic is executed. The logic for each tile of the GridMap is instantiated by code when the scene is loaded onto the tiles.

In this tutorial we will 
- start by creating the gridmap to draw a road network.
- see how we can create a straight path, on which a car will be instantiated and be able to follow a path.
- learn how to instantiate the straight path on a straight street through a script attached on the grid map. 
- see how the car will find it's next path by using a raycast on the car and Area3Ds on the paths, which will tell the car which new paths are available.
- finish the other road types and hope that cars are able to drive around. 

This tutorial will not include traffic lights or ai. 

#### 2 Creating the GridMap
First we need the **GridMap** with which we can build the road network. Here the traffic will take place. 

To make use of a **GridMap** node we need a MeshLibrary first. For this we can create a new scene in which we drag our desired street assets from the _FileSystem_ tab into. I created my own assets with Blender, but you can also find them online, e.g. on [kenney](https://kenney.nl/assets/city-kit-roads). After dragging all assets into the scene, we can adjust them a bit and rename them in the scene tab (because those names will show up when we use the GridMap later). Then we click on Scene → Convert To → MeshLibrary.

In the main scene we can use the MeshLibrary by adding a **GridMap** node and loading the newly created MeshLibrary under _Mesh Library_. In my case, the cell size must be adjusted to 10 x 1 x 10, because in Blender I've made them 10 meters by 10 meters. After loading the _MeshLibrary_ into the **GridMap** node, you will see our assets appear next to the _inspector_. Now we can simply draw a road network as If we'd play Sim City 4.

![gridmap](images/2_gridmap.png)

#### 3 Creating the first path: the straight street
Of course, nothing will happen on the streets yet because we still need our road users and paths. Let's start by creating a path for the straight street. To do that, we will use the **Path* node. For every kind of street tile (curves, straight streets, junctions etc.) we need to declear a path with a **Path** node in an separate scene. 

Let's start with the simple straight street. This scene I will name "straight_lanes.tscn". 

In this new scene I drag the straight street mesh (which we also used for the MeshLibrary). I recommend doing that so that you can see exactly how you have to create the path. Also center the mesh by resetting the Translation. Note that we'll make this mesh invisible later since we only want to instantiate _paths_ on our road network.

After that we can add the **Path** node and draw the path. At the top of the screen there is the option "Add Points", which will create the path between those points. With the help of these we can create a straight path which the road user will be able to follow. 

Make sure that the path is drawn correctly from all perspectives. In my case, for example, I want to have a straight, flat path on the right side of the street. The path should also start exactly at the beginning of the street tile and end exactly at the end on the other side. For this, it can help to shift the perspective and to use "Use Snap". 

![straight path](images/3_path.png)

#### 3 Creating a path user
Let's create a path user, which will enable us to actually see the path being used. There is a convinient node called **PathFollow** which is able to follow a path if the path is the _parent_ of the **PathFollow** node. I attach a car mesh to the **PathFollow** node so that we can actually see it following the Path of our straight street. If you do not have a fitting mesh right now, you can simply add a random mesh like a cube to visualize the **PathFollow**. 

If we change the _Offset_ from the **PathFollow** node in the _Inspector_, we can see that the car follows the path. With _H-Offset_ and _V-Offset_ we can further refine the position of the **PathFollow** node.

To automate this movement, we  can attach a script to the **PathFollow** node and write some temporary code:
```gdscript 
func _process(delta):
	offset = delta * 2
```
If we set up a camera we can now watch our **PathFollow** node following its path. 

Note that not only our straight path, but also our road users are instantiated through the straight street scene. Of course, this is also possible on other types of roads as well or through a completely different approach. I personally found it convinient to have the cars instantiated through the straight street scene.

#### 4 Adding lanes to the gridmap
Now we've created the path for the straight streets. Before we finish the paths for the other street tiles, I would like to instiate the straight street scene onto the **GridMap**. To do that, we will go back to our main scene and add a script to our **GridMap** which we used before to draw our road network.

To instantiate the straight street lanes, we can preload the corresponding scene in our script and save it in a variable. 

`onready var _straight_lanes = preload("res://scenes/straight_lanes.tscn")`

Now we need to let the **GridMap** know on which kind of street tiles it should instantiate the straight lanes, since we do not want those straight lanes on our curves etc. For this case we can use the [get_used_cells](https://docs.godotengine.org/en/stable/classes/class_gridmap.html#class-gridmap-method-get-used-cells) method from the **GridMap** node. This method gives us back all cells from the **GridMap** and which item of the MeshLibrary is used on it. 

If we click on the loaded MeshLibrary of our **GridMap** node in the inspector, Godot gives us a list of items, where we can see what kind of cell (in our case what kind of street) is assigned to which number. In my case, 0 is grass, 1  is crossing etc. 

![item list](images/9_itemlist.png)

We want to know which number the straight street is assigned to, which in my case is number 3. This we want to save in our script: `const STRAIGHT = 3`

Now we can declare that if a cell uses the cell item number 3 (which is the straight street as we now know), an instance of our preloaded straight street scene should be instantiated. 

```gdscript
onready var _straight_lanes = preload("res://scenes/straight_lanes.tscn")

const STRAIGHT = 3

func _ready():
	for cell in get_used_cells():
		if get_cell_item(cell.x, cell.y, cell.z) == STRAIGHT:
			_add_traffic_nodes(cell, _straight_lanes)


func _add_traffic_nodes(cell, traffic_node):
	var traffic_node_instance = traffic_node.instance()
	add_child(traffic_node_instance)
```
However, we also have to specify the position and rotation of our instantiated scene(s). For the position we use the [map_to_world](https://docs.godotengine.org/en/stable/classes/class_gridmap.html#class-gridmap-method-map-to-world) method, which "returns the position of a grid cell in the GridMap's local coordinate space."

```gdscript
func _add_traffic_nodes(cell, traffic_node):
	var traffic_node_instance = traffic_node.instance()
	traffic_node_instance.translation = map_to_world(cell.x, cell.y, cell.z)
	add_child(traffic_node_instance)
```
For the correct rotation of our paths, we go through the four cardinal directions with the [get_cell_item_orientation](https://docs.godotengine.org/en/stable/classes/class_gridmap.html#class-gridmap-method-map-to-world) method, which Godot returns 0, 10, 16, 22 (for reasons for me unknown). I store these numbers in variables.

```gdscript 
const NORTH = 10
const EAST = 16
const SOUTH = 0
const WEST = 22
```

For each cardinal direction, the tile is to be rotated by 90°. Depending on the number of tiles, this can be a bit tricky. You have to make sure that it fits your tiles. Otherwise you may have to adjust the orientation in your street scenes.

```gdscript
func _add_traffic_nodes(cell, traffic_node):
	var traffic_node_instance = traffic_node.instance()
	traffic_node_instance.translation = map_to_world(cell.x, cell.y, cell.z)
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == NORTH:
		traffic_node_instance.rotation_degrees.y += 180
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == EAST:
		traffic_node_instance.rotation_degrees.y += 90
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == SOUTH:
		traffic_node_instance.rotation_degrees.y += 0
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == WEST:
		traffic_node_instance.rotation_degrees.y += 270
	add_child(traffic_node_instance)
```

If you start the main scene now, you should be able to watch tiny meshes appearing and moving on all straight street tiles of your road network.

![straight path](images/4_instantiated.png)


#### 5 Finding the next Path

However, those meshes will be set back to the beginning of the path when they reach its end. This is because they do not know where they should heading next. Let's fix that by going back to our script for our **PathFollow** node in the _straight_street.tscn_. 

To let road users know that they need a new path, we first tell them that their current one has ended. We can check this by checking the _Unit Offset_, which represents the position on the path between 0 and 1. At 1, the **PathFollow** node has reached the end of the path.

I create a signal that is activated when the end of the path is reached. In this case it is important to turn off "Loop" for the path in the _inspector_, otherwise in my case Godot did not recognize when the PathFollow reached the _Unit Offset_ of 1.

```gdscript
func _process(delta):
	# Movement
	offset += car_velocity * delta
	if unit_offset == 1:
		emit_signal("path_end_reached")
```
When we connect the signal with our script, we can define what the **PathFollow** should do then. First, when the end of the path is reached, the **PathFollow** node must get rid of the old path. We find the old path by asking for the parent of the **PathFollow**, which should always be a path. 
```gdscript
func _on_AICar_path_end_reached():
	# Remove current path when end is reached
	var old_path = self.get_parent()
	old_path.remove_child(self)
```
Now the **PathFollow** node should attach itself onto a new path. For now, the new path is just a variable declared at the beginning of our script, but it will be more soon. Before we do that, we set _unit_offset_ to 0 when the path has ended, because this leads to a smooth transition between the paths.

```gdscript
var _new_path


func _on_AICar_path_end_reached():
	# Remove current path when end is reached
	var old_path = self.get_parent()
	old_path.remove_child(self)
	# Get new path
	if _new_path != null:
		_new_path.add_child(self)
	# Smoothing the transit
	unit_offset = 0
```

Where does the new path come from? Our road user will get the new path with the help of a **Raycast**, which is attached to the **PathFollow** node, and an **Area3D**, which gives information on the new path(s). It is important to note that in Godot 3 the default setting for **Raycasts* is that they are _not_ enabled, so we should change that. 

Now I want to be able to specifically ask for specific **Area3D** nodes, meaning our path transmitters, when the raycasts are colliding with them. For this reason we can add the **Area3Ds** to a specific _collision layer_. To do this, I first go to _Projects Setting_ and scroll down to _LayerNames_. Here I choose _3d Physics_, choose position 9 and call it "path_transmitter".

![raycast](./images/5_raycast.png)

Now we can get back to our script. We save the raycast in a variable: `onready var _raycast = $RayCast`

#### 6 Setting up the transmitters
The **Area3Ds** which will work as transmitters do not exist yet, so let's create them. Note that every street type not only needs its unique paths, but also those transmitters.

To do this, we go to our _straight_street_ scene and add an **Area3D** node. Then we add the necessary **CollisionShape**, which the car has to recognise. Thus it is mandatory that the **CollisionShape** is big enough so that it can be found by the raycast of the road user.

Since we created the path before, we need to reorder the hierarchy in our _SceneTree_. I recommend taking the **CollisionShape** as the first child of the **Area3D** node and all possible paths sequentially in second place. Have a look at this picture:

![transmitter](./images/6_collisionshape.png)

The order of the _SceneTree_ is important because the new path is chosen by asking for a child on a **specific position** of the **Area3D** node, which you can see in the code in the next chapter. 

#### 7 Finishing the PathFollow script
Back in the script, I store the number we chose for our **Area3D** transmitters in a variable: `const PATH_TRANSMITTER = 8`. Note that I take the number 8 instead of 9, since Godot starts at 0 when querying the layers. 

Now we can use the [is_colliding()](https://docs.godotengine.org/en/stable/classes/class_raycast.html#class-raycast-method-is-colliding) method of our raycast to check if the raycast collides with something.
```gdscript
	if _raycast.is_colliding():
		_check_raycast_collisions()
```

If this is the case, we activate a new method, which I call `_check_raycast_collision()`. We store the object that the raycast collides with in a variable. So far, there is only one object that the raycast can collide with, but if you want to add more logic later on, it will be useful to be able to ask for the collided object.
```gdscript
func _check_raycast_collisions():
	var _collision_object = _raycast.get_collider()
```
This allows us to check if the collided object is a __path_transmitter_. When we know that, we can therefore ask, how many paths are available for the _PathFollow_ node to take as a new path. We save this information in a variable `number_of_paths`. 
```gdscript
func _check_raycast_collisions():
	var _collision_object = _raycast.get_collider()
	if _collision_object.get_collision_layer_bit(PATH_TRANSMITTER):
		# Ask transmitter for number of paths
		var number_of_paths: int = 0 
```
Then we go through the children of the path_transmitter and ask how many of the children is a _Path_ node. We get back a number of paths that the car can choose from. Let's say the transmitter tells us there is only one path because it on a straight street with only one path. This enables us to fill our `_new_path` variable we declared at the beginning of the script with a new path: the path is a child of the transmitter on position 1. 

However, in case of more than one path, the **PathFollow** node can randomly choose which path to take.

```gdscript
func _check_raycast_collisions():
	var _collision_object = _raycast.get_collider()
	if _collision_object.get_collision_layer_bit(PATH_TRANSMITTER):
		# Ask transmitter for number of paths
		var number_of_paths: int = 0 
		for child in _collision_object.get_children():
			if child is Path:
				number_of_paths += 1
		# Get random path between 1 (always given) and number of paths
		_new_path = _collision_object.get_child(round(rand_range(1, number_of_paths)))
```
Have a look at the complete script:
```gdscript
extends PathFollow

signal path_end_reached
const PATH_TRANSMITTER = 8
export var car_velocity: float = 2
var _new_path
onready var _raycast = $RayCast


func _process(delta):
	# Movement
	offset += car_velocity * delta
	# Check for next path
	if _raycast.is_colliding():
		_check_raycast_collisions()
	if unit_offset == 1:
		emit_signal("path_end_reached")


func _on_AICar_path_end_reached():
	# Remove current path when end is reached
	var old_path = self.get_parent()
	old_path.remove_child(self)
	# Get new path
	if _new_path != null:
		_new_path.add_child(self)
	# Smoothing the transit
	unit_offset = 0


func _check_raycast_collisions():
	var _collision_object = _raycast.get_collider()
	if _collision_object.get_collision_layer_bit(PATH_TRANSMITTER):
		# Ask transmitter for number of paths
		var number_of_paths: int = 0 
		for child in _collision_object.get_children():
			if child is Path:
				number_of_paths += 1
		# Get random path between 1 (always given) and number of paths
		_new_path = _collision_object.get_child(round(rand_range(1, number_of_paths)))
```
#### 8 Finishing the other street types
When we hit the play button, we'll see that our PathFollow meshes are now able to find the next path, _if_ the next path is a straight street. Let's create the other street types to finally see the traffic flowing.

The principle of creating paths for other road types is the same. The curve, for example, has only one path in both direction like the straight street. However, in case of intersections we have to offer the car multiple paths to choose from. It's still easy to do, because we'll just add paths for every possible direction. Have a look at the three-way junction:

![transmitter](./images/7_threewayjunction.png)

In case of the four-way junction I simply created a separate scene with a transmitter and three possible paths, which I instantiated as a child in the four-way junction scene, where I'd put the child at each entrance of the junction. 

![transmitter](./images/8_fourwayjunction.png)

We will save those different street types in different scenes. Now we simply have to add them to the gridmap script to have them instatiated on their according tiles. Have a look at this complete script:

```gdscript
extends GridMap

onready var _straight_lanes = preload("res://scenes/straight_lanes.tscn")
onready var _curve_lanes = preload("res://scenes/curve_lanes.tscn")
onready var _t_crossing_lanes = preload("res://scenes/t_crossing_lanes.tscn")
onready var _crossing_lanes = preload("res://scenes/crossing_lanes.tscn")


const CROSSING = 1
const CURVE = 2 
const STRAIGHT = 3
const TCROSSING = 4

# Orientation
const NORTH = 10
const EAST = 16
const SOUTH = 0
const WEST = 22


func _ready():
	for cell in get_used_cells():
		if get_cell_item(cell.x, cell.y, cell.z) == STRAIGHT:
			_add_traffic_nodes(cell, _straight_lanes)
		if get_cell_item(cell.x, cell.y, cell.z) == CURVE:
			_add_traffic_nodes(cell, _curve_lanes)
		if get_cell_item(cell.x, cell.y, cell.z) == TCROSSING:
			_add_traffic_nodes(cell, _t_crossing_lanes)
		if get_cell_item(cell.x, cell.y, cell.z) == CROSSING:
			_add_traffic_nodes(cell, _crossing_lanes)


func _add_traffic_nodes(cell, traffic_node):
	var traffic_node_instance = traffic_node.instance()
	traffic_node_instance.translation = map_to_world(cell.x, cell.y, cell.z)
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == NORTH:
		traffic_node_instance.rotation_degrees.y += 180
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == EAST:
		traffic_node_instance.rotation_degrees.y += 90
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == SOUTH:
		traffic_node_instance.rotation_degrees.y += 0
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == WEST:
		traffic_node_instance.rotation_degrees.y += 270
	add_child(traffic_node_instance)
```

To check if all streets are instantiated correctly, it may be usefull to activate "Visible Collision Shapes" in the debug options of Godot, which will make visible our transmitters and raycasts. If we hit the play button now, our **PathFollow** nodes should now be able to drive around on the road network. 

#### 8 Car orientation
Probably the cars are wrongly orientated when they follow a curve path for instance. We can fix that by clicking on the **PathFollow** node and selecting "Oriented" in the "Rotation Mode" options. This may rotate your traffic user mesh in the _Inspector_, but this is easy to fix by simply readjusting the mesh again. See that the raycast is also oriented correctly. 

Now the cars should be able to drive around correctly. Download and check out the project to see this in action. 

#### 9 Future issues
I'll try to explain how I made traffic lights and wrote some basic car ai later on. For now I'd recommend to try out adding behaviour with the help of the raycasts. If the raycast hits another car, you can tell it to stop driving for instance. Traffic lights work the same: When the traffic light is red, I simply activate an **Area3D** node with a **CollisionShape**, which is recognised by the car and thus will stop driving. 
